<?php
define('MAX_LINE_LENGTH', 1024 * 1024);
$argList = $argv;
class BasicBot {
	protected $sock, $debug=false;
        protected $lane = 0, $pieces, $lastPiece=0, $curPiece=0, $nextPiece=1, $thirdPiece=2, $fourthPiece=3;
        protected $totalAngle=0, $throttle=1, $oldThrottle=0, $angle=0, $apexExit=false, $speed=0, $lastDistance=0, $cornerStart=false, $cornerEnd=0, $cornerLength=0, $nextCorner=0;
	function __construct($host, $port, $botname, $botkey) {
            global $argList;
            if (isset($argList[5])) {
                $this->debug = true;
            }
		$this->connect($host, $port, $botkey);
                if (!$this->debug) {
                    $this->write_msg('join', array(
                        'name' => $botname,
                        'key' => $botkey
                    ));
                } else {
                    $this->write_msg('joinRace', array(
                        'botId'=>array(
                            'name' => $botname,
                            'key' => $botkey
                        ),
                        'trackName'=>'keimola',
                        'carCount'=>1
                    ));
                }
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
                    $this->debug('** ' . $this->sockerror());
		}
                //$this->debug(rtrim($line));
		return json_decode($line);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		//$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n\n";
		}
	}
	
	public function run() {
            $exit = false;
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg->msgType) {
				case 'carPositions':
                                    foreach ($msg->data as $car) {
                                        if ($car->id->name === 'Double D Racing' || $car->id->name === 'SMG Design') {
                                            $this->oldThrottle = $this->throttle;
                                            $this->lane = $car->piecePosition->lane->startLaneIndex;
                                            $this->lastPiece = $this->curPiece;
                                            $this->curPiece = $car->piecePosition->pieceIndex;
                                            $this->nextPiece = $this->curPiece+1;
                                            $this->thirdPiece = $this->nextPiece+1;
                                            $this->fourthPiece = $this->thirdPiece+1;
                                            
                                            if (!isset($this->pieces[$this->nextPiece])) {
                                                $this->nextPiece = $this->nextPiece-(count($this->pieces));
                                            }
                                            if (!isset($this->pieces[$this->thirdPiece])) {
                                                $this->thirdPiece = $this->thirdPiece-(count($this->pieces));
                                            }
                                            if (!isset($this->pieces[$this->fourthPiece])) {
                                                $this->fourthPiece = $this->fourthPiece-(count($this->pieces));
                                            }
                                            
                                            $this->apexExit = false;
                                            
                                            if (property_exists($this->pieces[$this->curPiece], 'radius')) {
                                                if ($this->cornerStart === false) {
                                                    // started a new corner \\
                                                    $this->cornerStart = $this->curPiece;
                                                    $i = $this->curPiece;
                                                    while (property_exists($this->pieces[$i], 'radius')) {
                                                        $this->cornerLength += $this->pieces[$i]->radius;
                                                        $i++;
                                                        if (!isset($this->pieces[$i])) {
                                                            $i = 0;
                                                        }
                                                    }
                                                    $this->cornerEnd = $i;
                                                } else {
                                                    $offset = $this->cornerStart;
                                                    if ($this->curPiece-$offset >= $this->cornerEnd-$offset) {
                                                        // this is the end of the apex \\
                                                        $this->apexExit = true;
                                                        $this->throttle += 0.8;
                                                    }
                                                    $this->throttle += 0.4;
                                                }
                                            } else {
                                                $this->cornerStart = false;
                                            }
                                            if ($car->piecePosition->inPieceDistance < $this->lastDistance) {
                                                $this->lastDistance = -((!isset($this->pieces[$this->lastPiece]->length) ? $this->pieces[$this->lastPiece]->radius : $this->pieces[$this->lastPiece]->length) - $this->lastDistance);
                                            }
                                            $this->speed = $car->piecePosition->inPieceDistance - $this->lastDistance;
                                            $this->lastDistance = $car->piecePosition->inPieceDistance;
                                            if (property_exists($this->pieces[$this->nextPiece], 'radius')) {
                                                if ($this->pieces[$this->nextPiece]->angle > 35 || $this->pieces[$this->nextPiece]->angle < -35 || $this->speed > 5) {
                                                    if ($this->throttle > 0.5 || ($this->angle > 45 || $this->angle < -45) || $this->speed > 4) {
                                                        $this->throttle = $this->throttle-0.5;
                                                    }
                                                } else {
                                                    if ($this->throttle < 0.5) {
                                                        $this->throttle += 0.5;
                                                    } else {
                                                        $this->throttle += 0.3;
                                                    }
                                                }
                                                $this->nextCorner = 1;
                                            }
                                            if (property_exists($this->pieces[$this->thirdPiece], 'radius')) {
                                                if ($this->pieces[$this->thirdPiece]->angle > 35 || $this->pieces[$this->thirdPiece]->angle < -35 || $this->speed > 6) {
                                                    if ($this->throttle > 0.6 || ($this->angle > 45 || $this->angle < -45) || $this->speed > 5) {
                                                        $this->throttle = $this->throttle-0.4;
                                                    }
                                                } else {
                                                    if ($this->throttle < 0.5) {
                                                        $this->throttle += 0.5;
                                                    } else {
                                                        $this->throttle += 0.3;
                                                    }
                                                }
                                                $this->nextCorner = 2;
                                            }
                                            if (property_exists($this->pieces[$this->fourthPiece], 'radius')) {
                                                if ($this->pieces[$this->fourthPiece]->angle > 35 || $this->pieces[$this->fourthPiece]->angle < -35 || $this->speed > 6) {
                                                    if ($this->throttle > 0.7 || ($this->angle > 45 || $this->angle < -45) || $this->speed > 7) {
                                                        $this->throttle = $this->throttle-0.3;
                                                    }
                                                } else {
                                                    if ($this->throttle < 0.5) {
                                                        $this->throttle += 0.6;
                                                    } else {
                                                        $this->throttle += 0.4;
                                                    }
                                                }
                                                $this->nextCorner = 3;
                                            }
                                            if ($car->angle > 0 && $this->angle > 0 && $car->angle > $this->angle || $car->angle < 0 && $this->angle < 0 && $car->angle < $this->angle) {
                                                if ($this->throttle >= $this->oldThrottle) {
                                                    $this->throttle = $this->throttle-0.3;
                                                }
                                            } else if (property_exists($this->pieces[$this->curPiece], 'length') && $this->throttle == $this->oldThrottle) {
                                                $this->throttle += 0.6;
                                            }
                                            $this->angle = $car->angle;
                                            
                                            
                                            if (!property_exists($this->pieces[$this->curPiece], 'length')) {
                                                if ($this->apexExit) {
                                                    if ($this->pieces[$this->curPiece]->angle > 35 || $this->pieces[$this->curPiece]->angle < -35) {
                                                        //if ($this->throttle < 0.4) {
                                                            $this->throttle += 0.5;
                                                        //}
                                                    } else {
                                                        $this->throttle = 0.7;
                                                    }
                                                }
                                                if ($this->angle > 50 || $this->angle < -50) {
                                                    if ($this->throttle > 0.4) {
                                                        $this->throttle = 0;
                                                    }
                                                }
                                            }
                                            if (property_exists($this->pieces[$this->nextPiece], 'switch')) {
                                                if (property_exists($this->pieces[$this->thirdPiece], 'radius')) {
                                                    // started a new corner \\
                                                    $i = $this->thirdPiece;
                                                    $angle = 0;
                                                    while (property_exists($this->pieces[$i], 'radius')) {
                                                        $angle = $angle + $this->pieces[$i]->angle;
                                                        $i++;
                                                        if (!isset($this->pieces[$i])) {
                                                            $i = 0;
                                                        }
                                                    }
                                                    if ($angle < 0) {
                                                        $this->write_msg('switchLane', 'Left');
                                                    } else {
                                                        $this->write_msg('switchLane', 'Right');
                                                    }
                                                }
                                            }

                                            if (($this->angle > 35 && $car->angle > 5) || ($this->angle < -35 && $car->angle < -5) && $this->speed < 2) {
                                                if ($this->throttle < 0.3) {
                                                    $this->throttle += 0.2;
                                                }
                                                if ($this->nextCorner == 0) {
                                                    //$this->throttle = 1;
                                                } else {
                                                    //$this->throttle += +(0.5/-$this->nextCorner)/10;
                                                }
                                            } else if (($this->angle > 25 && $car->angle < 5) || ($this->angle < -25 && $car->angle > -5) && $this->speed < 3) {
                                                if ($this->throttle < 0.4) {
                                                    $this->throttle += 0.3;
                                                }
                                                if ($this->nextCorner == 0) {
                                                    //$this->throttle = 1;
                                                } else {
                                                    //$this->throttle += +(0.7/-$this->nextCorner)/10;
                                                }
                                            } else if (($this->angle > 10 && $car->angle < 5) || ($this->angle < -10 && $car->angle > -5) && $this->speed < 4) {
                                                if ($this->throttle < 0.5) {
                                                    $this->throttle += 0.4;
                                                }
                                                if ($this->nextCorner == 0) {
                                                    //$this->throttle = 1;
                                                } else {
                                                    //$this->throttle += +(0.9/-$this->nextCorner)/10;
                                                }
                                            } else if (($this->angle > 0 && $car->angle < 5) || ($this->angle < 0 && $car->angle > -5) && $this->speed < 6) {
                                                if ($this->throttle < 0.6) {
                                                    $this->throttle += 0.4;
                                                }
                                                if ($this->nextCorner == 0) {
                                                    //$this->throttle = 1;
                                                } else {
                                                    //$this->throttle += +(1/-$this->nextCorner)/10;
                                                }
                                            }

                                            if ($this->throttle > 1) {
                                                $this->throttle = 1;
                                            } else if ($this->throttle < 0) {
                                                $this->throttle = 0;
                                            }




                                            // need to check the greater the speed the further ahead on the track \\
                                            /*if ($this->speed > 7) {
                                                $this->throttle = $this->throttle/($this->speed/5);
                                            }*/

                                            $this->write_msg('throttle', $this->throttle);
                                            if ($this->debug) {
                                                print_r("Throttle:\r\n");
                                                print_r($this->throttle."\r\n");
                                                print_r("Speed:\r\n");
                                                print_r($this->speed."\r\n");
                                                print_r("Current Item:\r\n");
                                                print_r($this->pieces[$this->curPiece]);
                                                print_r("Current Position:\r\n");
                                                print_r($msg->data);
                                            }
                                        }
                                    }
                                    break;
				case 'joinRace':
                                    
                                    break;
				case 'yourCar':
                                    
                                    break;
				case 'gameInit':
                                    $this->pieces = $msg->data->race->track->pieces;
                                    $this->curPiece = 0;
                                    foreach ($this->pieces as $piece) {
                                        if (property_exists($piece, 'angle')) {
                                            $this->totalAngle += $piece->angle;
                                        }
                                    }
                                    break;
				case 'gameStart':
                                    
                                    break;
				case 'crash':
                                    if ($this->debug) {
                                        print_r("Crash:\r\n");
                                        print_r($msg->data);
                                        print_r("\r\n");
                                    }
                                    break;
				case 'spawn':
				case 'lapFinished':
                                    break;
				case 'dnf':
                                    
                                    break;
				case 'finish':
                                    $exit = true;
                                    break;
				default:
					$this->write_msg('ping', null);
			}
                        if ($exit) {
                            break;
                        }
		}
	}
}
?>